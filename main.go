package main

import (
	"flag"

	"gitlab.com/ishmamabid99/techinal-assesment/api"
	"gitlab.com/ishmamabid99/techinal-assesment/config"
	"gitlab.com/ishmamabid99/techinal-assesment/etl"
)

func main() {
	if config.Db == nil {
		err := config.DbConfig()
		if err != nil {
			panic(err)
		}
	}
	runEtlFlags := flag.Bool("run-etl", false, "Run ETL")
	flag.Parse()
	if *runEtlFlags {
		err := etl.Run()
		if err != nil {
			panic(err)
		}
	} else {
		err := api.Run()
		if err != nil {
			panic(err)
		}
	}
}
