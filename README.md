# Food Delivery Platform Backend API && ETL

This project builds a Go API server with a PostgreSQL database to support a food delivery platform. It includes functionalities for managing restaurants, users, and their interactions.

Technologies:

Backend: Go (using a framework like Gin)
Database: PostgreSQL
Documentation: Swagger
Containerization: Docker

## API Endpoints

`GET api/v1/restaurants`

Lists all restaurants available

`GET api/v1/filter-restaurants?more_or_less={query}&dish_count={x}&min_price{query}&max_price={query}&limit={y}`

 Lists all top restaurants having dish count more or less than x and price in a range of min and max with a limit of y

`GET api/v1/search?name={query}`

 Searches for restaurants or dishes by name, ranked by relevance to the search term.

`GET api/v1/open-restaurants?day={x}&time={y}`

Lists all the restaurants that are open in x day of the week at y time

`POST api/v1/purchase`

Processes a user purchasing a dish from a restaurant. Updates both user and restaurant cash balances in an atomic transaction.

## ETL Process

The script will read the raw data files:
`restaurant_with_menu.json`

`users_with_purchase_history.json`

convert them into go structures process them by batch and load them in databse in a batch size of 100.

## Getting Started

`$ chmod +x ./start.sh`

`$ chmod +x ./stop.sh`

`$ chmod +x ./run-etl.sh`

### Spin up the server

`$ ./start.sh`

### Stop the server

`$ ./stop.sh`

### Run ETL Service

`$ ./run-etl.sh`

### Api Documentation

`http://localhost:8080/swagger/index.html`
