package config

import (
	"fmt"
	"log"

	"gitlab.com/ishmamabid99/techinal-assesment/api/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var Db *gorm.DB

func DbConfig() error {
	config := map[string]string{
		"host":     "db",
		"port":     "5432",
		"user":     "myuser",
		"password": "mypass",
		"dbname":   "mydb",
	}
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=disable", config["host"], config["user"], config["password"], config["dbname"])
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}
	Db = db
	err = Db.AutoMigrate(
		&models.Restaurant{},
		&models.Dish{},
		&models.User{},
		&models.Purchase{},
		&models.OpeningHours{},
	)
	if err != nil {
		return err
	}
	log.Println("Database connected")
	return nil
}
