package controller

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ishmamabid99/techinal-assesment/api/models"
	"gitlab.com/ishmamabid99/techinal-assesment/config"
	"gorm.io/gorm"
)

type PurchaseBody struct {
	UserID    string         `json:"user_id"`
	Purchases []UserPurchase `json:"purchases"`
}
type UserPurchase struct {
	RestaurantID string `json:"restaurant_id"`
	DishID       string `json:"dish_id"`
	Qty          int    `json:"qty"`
}

// PurchaseDish

// @Summary		Purchase dish
// @Description	Purchase dish
// @Tags			purchase
// @Accept			json
// @Produce		json
// @Param			purchase	body	PurchaseBody	true	"Purchase body"
// @Success		200			json	{message: "Purchase successful"}
// @Router			/purchase [post]
func PurchaseDish(c *gin.Context) {

	body, err := io.ReadAll(c.Request.Body)
	if err != nil {
		log.Println(err)
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}
	var purchaseBody PurchaseBody
	if err := json.Unmarshal(body, &purchaseBody); err != nil {
		handleErr(c, err)
		return
	}

	var user models.User
	err = config.Db.First(&user, purchaseBody.UserID).Error
	if err != nil {
		handleErr(c, err)
		return
	}
	// 	restaurant.CashBalance += transactionAmount
	// 	utils.Db.Save(&user)
	// 	utils.Db.Save(&restaurant)
	// }
	err = PerformPurchaseDBTransaction(config.Db, user, purchaseBody.Purchases)
	if err != nil {
		handleErr(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Purchase successful"})
}

func PerformPurchaseDBTransaction(db *gorm.DB, user models.User, userPurchases []UserPurchase) error {
	tx := db.Begin()
	for _, userPurchase := range userPurchases {
		purchase, err := validateAndFormatPurchase(user, userPurchase)
		if err != nil {
			tx.Rollback()
			return err
		}
		if err := tx.Create(&purchase).Error; err != nil {
			tx.Rollback()
			return err
		}
		user.CashBalance -= purchase.TransactionAmount
		if err := tx.Save(&user).Error; err != nil {
			tx.Rollback()
			return err
		}
		restaurant, err := models.FindRestaurantByID(tx, userPurchase.RestaurantID)
		if err != nil {
			tx.Rollback()
			return err
		}
		restaurant.CashBalance += purchase.TransactionAmount
		if err := tx.Save(&restaurant).Error; err != nil {
			tx.Rollback()
			return err
		}

	}
	return tx.Commit().Error
}

func validateAndFormatPurchase(user models.User, userPurchase UserPurchase) (models.Purchase, error) {
	if userPurchase.Qty <= 0 {
		return models.Purchase{}, errors.New("invalid quantity")
	}
	var dish models.Dish
	dish, err := models.FindDishByID(config.Db, userPurchase.DishID)
	if err != nil {
		return models.Purchase{}, errors.New("invalid dish id")
	}
	var restaurant models.Restaurant
	restaurant, err = models.FindRestaurantByID(config.Db, userPurchase.RestaurantID)
	if err != nil {
		return models.Purchase{}, errors.New("invalid restaurant id")
	}
	if user.CashBalance < dish.Price*float64(userPurchase.Qty) {
		return models.Purchase{}, errors.New("insufficient balance")
	}

	return models.Purchase{
		UserID:            user.ID,
		RestaurantName:    restaurant.Name,
		DishName:          dish.Name,
		TransactionDate:   time.Now(),
		TransactionAmount: dish.Price * float64(userPurchase.Qty),
	}, nil
}

func handleErr(c *gin.Context, err error) {
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
	}
}
