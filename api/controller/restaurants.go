package controller

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/ishmamabid99/techinal-assesment/api/models"
	"gitlab.com/ishmamabid99/techinal-assesment/config"
)

type RestaurantsResponse struct {
	ID   uint
	Name string
}

var mapDay = map[string]time.Weekday{
	"Sunday":    time.Sunday,
	"Monday":    time.Monday,
	"Tuesday":   time.Tuesday,
	"Wednesday": time.Wednesday,
	"Thursday":  time.Thursday,
	"Friday":    time.Friday,
	"Saturday":  time.Saturday,
}

// GetRestaurants
// @Summary		Get all restaurants
// @Description	Get all restaurants
// @Tags			restaurants
// @Accept			json
// @Produce			json
// @Success		200	json  RestaurantResponse
// @Router			/restaurants [get]
func GetRestaurants(c *gin.Context) {
	var restaurants []models.Restaurant
	err := config.Db.Find(&restaurants).Error
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}
	var response []RestaurantsResponse
	for _, restaurant := range restaurants {
		response = append(response, RestaurantsResponse{
			ID:   restaurant.ID,
			Name: restaurant.Name,
		})
	}
	c.JSON(http.StatusOK, gin.H{"data": response})
}

// GetOpenRestaurants

// @Summary		Get open restaurants
// @Description	Get open restaurants
// @Tags			restaurants
// @Accept			json
// @Produce			json
// @Param			day	query	string	true	"Day (example: Saturday) note: case sensitive"
// @Param			time	query	string	true	"Time (example: 15:04) note: 24 hour format"
// @Success		200	json	 OpenResponse
// @Router			/open-restaurants [get]
func GetOpenRestaurants(c *gin.Context) {
	day := c.Query("day")
	timeStr := c.Query("time")
	targetTime, err := time.Parse("15:04", timeStr)
	if err != nil {
		handleErr(c, errors.New("invalid value for 'time'"))
		return
	}
	weekday, ok := mapDay[day]
	if !ok {
		handleErr(c, errors.New("invalid value for 'day'"))
		return
	}
	log.Println(weekday, targetTime)
	queryStr :=
		`SELECT r.name, r.id, oh.day, oh.opening_time, oh.closing_time
	FROM restaurants r
	INNER JOIN opening_hours oh ON r.id = oh.restaurant_id
	WHERE oh.day = $1
	AND CAST($2 AS TIME) BETWEEN CAST(oh.opening_time AS TIME) AND CAST(oh.closing_time AS TIME);`
	var openRestaurants []struct {
		RestaurantsResponse
		Day         time.Weekday
		OpeningTime string
		ClosingTime string
	}
	err = config.Db.Raw(queryStr, weekday, targetTime).Scan(&openRestaurants).Error

	if err != nil {
		handleErr(c, err)
		return
	}
	// if len(openRestaurants) == 0 {
	// 	fmt.Println("No restaurants are open at", targetTime.Format("Monday, 03:04PM")) // Use formatted time
	// }

	c.JSON(http.StatusOK, gin.H{"data": openRestaurants})
}

// GetTopYRestaurants

// @Summary		Get top Y restaurants
// @Description	Retrieves the top Y restaurants based on a specific criteria (implementation details required)
// @Tags			restaurants
// @Accept			json
// @Produce		json
// @Param			more_or_less	query	string	true	"More or less represented by > or < example: >"
// @Param			dish_count	query	int	true	"Number of dishes example: 10"
// @Param			min_price	query	number	true	"Minimum price example: 10"
// @Param			max_price	query	number	true	"Maximum price example: 100"
// @Param			limit	query	int	true	"Limits the number of results example: 10"
// @Success		200	json	{data: [{id: 1, name: "Restaurant 1", dish_count: 10, min_price: 10, max_price: 100}]}
// @Router			/filter-restaurants [get]
func GetTopYRestaurants(c *gin.Context) {
	moreLess := c.Query("more_or_less")
	dish_count, err := strconv.Atoi(c.Query("dish_count"))
	if err != nil {
		handleErr(c, errors.New("invalid value for filter"))
		return
	}
	minPrice, err := strconv.ParseFloat(c.Query("min_price"), 64)
	if err != nil {
		handleErr(c, errors.New("invalid value for 'min_price'"))
		return
	}
	maxPrice, err := strconv.ParseFloat(c.Query("max_price"), 64)
	if err != nil {
		handleErr(c, errors.New("invalid value for 'max_price'"))
		return
	}
	limit, err := strconv.Atoi(c.Query("limit"))
	if err != nil {
		handleErr(c, errors.New("invalid value for limit"))
		return
	}
	queryStr := fmt.Sprintf(
		`SELECT r.id, r.name, COUNT(d.id) 
	as dish_count, MIN(d.price) as min_price, MAX(d.price) as max_price
	FROM restaurants r
	INNER JOIN dishes d ON r.id = d.restaurant_id
	GROUP BY r.id
	HAVING COUNT(d.id) %s ($1)
	AND MIN(d.price) > $2 AND MAX(d.price) < $3
	ORDER BY r.name ASC
	LIMIT $4	
  `, moreLess)

	var restaurants []struct {
		RestaurantsResponse
		DishCount int
		MinPrice  float64
		MaxPrice  float64
	}
	err = config.Db.Raw(queryStr, dish_count, minPrice, maxPrice, limit).Scan(&restaurants).Error
	if err != nil {
		handleErr(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": restaurants})
}

// SearchRestaurants

// @Summary		Search restaurants
// @Description	Search restaurants
// @Tags			restaurants
// @Accept			json
// @Produce			json
// @Param			name	query	string	true	"Name of the restaurant or dish example: Restaurant 1"
// @Success		200	json	{data: [{id: 1, name: "Restaurant 1", dish_name: "Dish 1", dish_price: 10, rank: 0.5}]}
// @Router			/search [get]
func SearchRestaurants(c *gin.Context) {
	name := c.Query("name")
	var restaurants []struct {
		RestaurantsResponse
		DishName  string
		DishPrice float64
		Rank      float64
	}
	queryStr := fmt.Sprintf(
		`SELECT r.id, r.name, d.name as dish_name, d.price as dish_price
	, ts_rank(to_tsvector('english', r.name || ' ' || d.name),
	to_tsquery('english', '''%s''')) as rank
	FROM restaurants r
	LEFT JOIN dishes d ON r.id = d.restaurant_id
	WHERE to_tsvector('english', r.name || ' ' || d.name)
	 @@ to_tsquery('english', '''%s''')
	ORDER BY rank DESC`, name, name)
	err := config.Db.Raw(queryStr, "%"+name+"%").Scan(&restaurants).Error
	if err != nil {
		handleErr(c, err)
		return
	}
	c.JSON(http.StatusOK, gin.H{"data": restaurants})
}
