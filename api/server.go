package api

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/ishmamabid99/techinal-assesment/api/routes"
	"gitlab.com/ishmamabid99/techinal-assesment/docs"
)

func Run() error {

	router := gin.New()
	docs.SwaggerInfo.BasePath = "/api/v1"
	router.Use(gin.Logger())
	v1 := router.Group("/api/v1")
	routes.Routes(v1)
	router.GET("/api/v1/health", func(c *gin.Context) {
		log.Println("Health check")
		data := gin.H{"status": "ok", "message": "Health check", "time": time.Now().Format(time.RFC3339)}
		c.JSON(http.StatusOK, data)
	})
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	router.Run(":8080")
	return nil
}
