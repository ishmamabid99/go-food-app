package models

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name        string     `json:"name"`
	CashBalance float64    `json:"cash_balance"`
	Purchases   []Purchase `json:"-" gorm:"foreignKey:UserID"`
}

func FindUserByID(db *gorm.DB, id string) (User, error) {
	var user User
	err := db.First(&user, id).Error
	return user, err
}
