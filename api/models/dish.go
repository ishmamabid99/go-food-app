package models

import (
	"gorm.io/gorm"
)

type Dish struct {
	gorm.Model
	RestaurantID uint    `json:"-" gorm:"foreignKey:RestaurantID"`
	Name         string  `json:"name"`
	Price        float64 `json:"price"`
}

func FindDishByID(db *gorm.DB, id string) (Dish, error) {
	var dish Dish
	err := db.First(&dish, id).Error
	return dish, err
}
