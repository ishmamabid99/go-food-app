package models

import (
	"time"

	"gorm.io/gorm"
)

type Restaurant struct {
	gorm.Model
	ID           uint           `gorm:"primaryKey"`
	CashBalance  float64        `json:"cash_balance"`
	Name         string         `json:"name"`
	Dishes       []Dish         `json:"-" gorm:"foreignKey:RestaurantID"`
	OpeningHours []OpeningHours `json:"-" gorm:"foreignKey:RestaurantID"`
}
type OpeningHours struct {
	gorm.Model
	RestaurantID uint         `gorm:"foreignKey:RestaurantID;reference:restaurants(id)"`
	Day          time.Weekday `json:"day"`
	OpeningTime  string       `json:"opening_time"`
	ClosingTime  string       `json:"closing_time"`
}

func FindRestaurantByID(db *gorm.DB, id string) (Restaurant, error) {
	var restaurant Restaurant
	err := db.First(&restaurant, id).Error
	return restaurant, err
}
