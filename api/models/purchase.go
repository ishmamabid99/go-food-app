package models

import (
	"time"

	"gorm.io/gorm"
)

type Purchase struct {
	gorm.Model
	UserID            uint      `json:"-" gorm:"foreignKey:UserID;reference:users(id)"`
	RestaurantName    string    `json:"restaurant_name"`
	DishName          string    `json:"dish_name"`
	TransactionAmount float64   `json:"transaction_amount"`
	TransactionDate   time.Time `json:"transaction_date"`
}
