package routes

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/ishmamabid99/techinal-assesment/api/controller"
)

func Routes(
	router *gin.RouterGroup,
) {
	router.GET("/restaurants", controller.GetRestaurants)
	router.GET("/search", controller.SearchRestaurants)
	router.GET("/filter-restaurants", controller.GetTopYRestaurants)
	router.GET("/open-restaurants", controller.GetOpenRestaurants)

	router.POST("/purchase", controller.PurchaseDish)
}
