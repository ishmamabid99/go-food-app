package etl

import (
	"encoding/json"
	"io"
	"log"
	"os"

	"gitlab.com/ishmamabid99/techinal-assesment/api/models"
	"gitlab.com/ishmamabid99/techinal-assesment/config"
)

type JsonRestaurant struct {
	CashBalance float64 `json:"cashBalance"`
	Menu        []struct {
		DishName string  `json:"dishName"`
		Price    float64 `json:"price"`
	}
	RestaurantName string `json:"restaurantName"`
	OpeningHours   string `json:"openingHours"`
}

func ExtractRestaurant(filepath string) ([]JsonRestaurant, error) {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	defer file.Close()
	var data []byte
	data, err = io.ReadAll(file)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	jsonData := []byte(data)
	var extractedData []JsonRestaurant
	err = json.Unmarshal(jsonData, &extractedData)
	if err != nil {
		log.Fatal(err)
		return nil, err
	}
	return extractedData, nil

}

func TransformRestaurant(restaurants []JsonRestaurant) []models.Restaurant {
	var restaurantsModel []models.Restaurant
	for _, restaurant := range restaurants {
		var dishes []models.Dish
		for _, dish := range restaurant.Menu {
			dishes = append(dishes, models.Dish{
				Name:  dish.DishName,
				Price: dish.Price,
			})
		}
		openingHours := ParseOpeningHours(restaurant.OpeningHours)
		var openingHoursModel []models.OpeningHours
		for _, openingHour := range openingHours {
			openingHoursModel = append(openingHoursModel, models.OpeningHours{
				Day:         openingHour.Day,
				OpeningTime: openingHour.OpeningTime,
				ClosingTime: openingHour.ClosingTime,
			})
		}
		restaurantsModel = append(restaurantsModel, models.Restaurant{
			Name:         restaurant.RestaurantName,
			CashBalance:  restaurant.CashBalance,
			Dishes:       dishes,
			OpeningHours: openingHoursModel,
		})
	}
	return restaurantsModel
}

func LoadBatchOfRestaurants(restaurants []models.Restaurant) error {
	batchSize := 100
	for i := 0; i < len(restaurants); i += batchSize {
		end := i + batchSize
		if end > len(restaurants) {
			end = len(restaurants)
		}
		batch := restaurants[i:end]

		err := config.Db.CreateInBatches(batch, batchSize).Error
		if err != nil {
			log.Println("Error creating users in batch:", err)
			return err
		}
	}
	return nil
}

func RunRestaurantEtl() error {
	extractedData, err := ExtractRestaurant("./raw-data/restaurant_with_menu.json")
	if err != nil {
		return err
	}
	restaurants := TransformRestaurant(extractedData)
	err = LoadBatchOfRestaurants(restaurants)
	if err != nil {
		return err
	}
	return nil
}
