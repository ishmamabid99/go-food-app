package etl

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"time"

	"gitlab.com/ishmamabid99/techinal-assesment/api/models"
	"gitlab.com/ishmamabid99/techinal-assesment/config"
)

type JsonPurchase struct {
	Id              uint    `json:"id"`
	Name            string  `json:"name"`
	CashBalance     float64 `json:"cashBalance"`
	PurchaseHistory []struct {
		DishName          string  `json:"dishName"`
		RestaurantName    string  `json:"restaurantName"`
		TransactionAmount float64 `json:"transactionAmount"`
		TransactionDate   string  `json:"transactionDate"`
	} `json:"purchaseHistory"`
}

func ExtractUserData(filepath string) ([]JsonPurchase, error) {
	file, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	data, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}
	jsonData := []byte(data)
	var extractedData []JsonPurchase
	err = json.Unmarshal(jsonData, &extractedData)
	if err != nil {
		return nil, err
	}
	return extractedData, nil
}

func TransformUserPurchase(purchases []JsonPurchase) ([]models.User, error) {
	var users []models.User
	for _, purchase := range purchases {
		var user models.User
		user.Name = purchase.Name
		user.CashBalance = purchase.CashBalance
		for _, purchaseHistory := range purchase.PurchaseHistory {
			t, err := time.Parse("04/02/2006 03:04 PM", purchaseHistory.TransactionDate)
			if err != nil {
				log.Println("Error parsing date:", err)
				continue
			}
			user.Purchases = append(user.Purchases, models.Purchase{
				RestaurantName:    purchaseHistory.RestaurantName,
				DishName:          purchaseHistory.DishName,
				TransactionAmount: purchaseHistory.TransactionAmount,
				TransactionDate:   t,
			})
		}
		users = append(users, user)
	}
	return users, nil
}
func LoadBatchOfUsers(users []models.User) error {
	batchSize := 100 // Adjust this based on your needs
	for i := 0; i < len(users); i += batchSize {
		end := i + batchSize
		if end > len(users) {
			end = len(users)
		}
		batch := users[i:end]

		err := config.Db.CreateInBatches(batch, batchSize).Error
		if err != nil {
			log.Println("Error creating users in batch:", err)
			return err
		}
	}
	return nil
}

func RunUserEtl() error {
	extractedData, err := ExtractUserData("./raw-data/users_with_purchase_history.json")
	if err != nil {
		return err
	}
	users, err := TransformUserPurchase(extractedData)
	if err != nil {
		return err
	}
	err = LoadBatchOfUsers(users)
	if err != nil {
		return err
	}
	return nil
}
