package etl

import "log"

func Run() error {
	err := RunRestaurantEtl()
	if err != nil {
		return err
	}
	err = RunUserEtl()
	if err != nil {
		return err
	}
	log.Println("ETL ran successfully")
	return nil
}
