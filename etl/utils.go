package etl

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const regexContinuous = `^([A-Za-z]+)\s-\s([A-Za-z]+)\s+([0-9]{1,2}(?::[0-9]{2})?\s*[ap]m)\s-\s([0-9]{1,2}(?::[0-9]{2})?\s*[ap]m)`
const regexMultipleDays = `(?P<weekdays>\b[A-Za-z]+(?:,\s+[A-Za-z]+)*\b)\s+(?P<openingTime>[0-9]{1,2}(?::[0-9]{2})?\s*[ap]m)\s-\s(?P<closingTime>[0-9]{1,2}(?::[0-9]{2})?\s*[ap]m)`

var weekDays = map[string]time.Weekday{
	"SUN": time.Sunday,
	"MON": time.Monday,
	"TUE": time.Tuesday,
	"WED": time.Wednesday,
	"THU": time.Thursday,
	"FRI": time.Friday,
	"SAT": time.Saturday,
}

type OpeningHour struct {
	Day         time.Weekday
	OpeningTime string
	ClosingTime string
}

func formatTime(timeStr string) (string, error) {
	if strings.Contains(timeStr, ":") {
		return timeStr, nil
	}
	timeStr = strings.ReplaceAll(timeStr, " ", "")
	hourStr := timeStr[:len(timeStr)-2]
	hour, err := strconv.Atoi(hourStr)
	if err != nil {
		return "", err
	}
	meridiem := timeStr[len(timeStr)-2:]
	var t time.Time
	if meridiem == "am" {
		t = time.Date(2000, 1, 1, hour, 0, 0, 0, time.UTC)
	} else if meridiem == "pm" {
		t = time.Date(2000, 1, 1, hour+12, 0, 0, 0, time.UTC)
	} else {
		return "", fmt.Errorf("invalid meridiem: %s", meridiem)
	}
	formattedTime := t.Format("3:04 pm")
	return formattedTime, nil
}
func ParseContiguousDays(rawData string) []OpeningHour {
	// Write your code here
	var openingHours []OpeningHour
	match, err := regexp.MatchString(regexContinuous, rawData)
	if err != nil {
		log.Fatal(err)
	}
	if match {
		groups := regexp.MustCompile(regexContinuous).FindStringSubmatch(rawData)
		ot, err := formatTime(groups[3])
		if err != nil {
			log.Fatal(err)
		}
		ct, err := formatTime(groups[4])
		if err != nil {
			log.Fatal(err)
		}
		// openingTime, err := time.Parse("3:04 pm", ot)
		if err != nil {
			log.Fatal(err)
		}
		// closeTime, err := time.Parse("3:04 pm", ct)
		if err != nil {
			log.Fatal(err)
		}
		startDay := weekDays[strings.ToUpper(groups[1][0:3])]
		endDay := weekDays[strings.ToUpper(groups[2][0:3])]
		currentDay := startDay
		for {
			openingHour := OpeningHour{
				Day:         currentDay,
				OpeningTime: ot,
				ClosingTime: ct,
			}
			openingHours = append(openingHours, openingHour)
			if currentDay == endDay {
				break
			}
			currentDay = (currentDay + 1) % 7
		}
	}
	return openingHours
}

func ParseMultipleDays(rawData string) []OpeningHour {
	// Write your code here
	var openingHours []OpeningHour
	match, err := regexp.MatchString(regexMultipleDays, rawData)
	if err != nil {
		log.Fatal(err)
	}
	if match {
		groups := regexp.MustCompile(regexMultipleDays).FindStringSubmatch(rawData)
		weekdays := strings.Split(groups[1], ",")
		ot, err := formatTime(groups[2])
		if err != nil {
			log.Fatal(err)
		}
		ct, err := formatTime(groups[3])
		if err != nil {
			log.Fatal(err)
		}
		// Parse opening and closing times
		// openingTime, err := time.Parse("3:04 pm", openingTimeStr)
		if err != nil {
			log.Fatal(err)
		}
		// closingTime, err := time.Parse("3:04 pm", closingTimeStr)
		if err != nil {
			log.Fatal(err)
		}
		for _, weekday := range weekdays {
			weekday = strings.ReplaceAll(weekday, " ", "")
			openingHour := OpeningHour{
				Day:         weekDays[strings.ToUpper(weekday[0:3])],
				OpeningTime: ot,
				ClosingTime: ct,
			}
			openingHours = append(openingHours, openingHour)
		}

	}
	return openingHours
}
func ParseOpeningHours(rawData string) []OpeningHour {
	// rawData := "Sat - Mon 2:45 pm - 11:15 pm / Tues, Thurs 4 pm - 8:30 pm / Weds 6:45 am - 11 pm / Fri 10:45 am - 2:45 am"
	openingHours := make([]OpeningHour, 0)
	splitData := strings.Split(rawData, "/")
	for _, data := range splitData {
		field := strings.TrimSpace(data)
		isContinuing := regexp.MustCompile(regexContinuous).MatchString(field)
		isMultipleDays := regexp.MustCompile(regexMultipleDays).MatchString(field)
		if isContinuing {
			openingHours = append(openingHours, ParseContiguousDays(field)...)
		} else if isMultipleDays {
			openingHours = append(openingHours, ParseMultipleDays(field)...)
		} else {
			log.Fatal("Invalid data")
		}
	}
	return openingHours
}
